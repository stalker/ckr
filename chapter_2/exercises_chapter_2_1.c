/* $Date$
 * $Id$
 * $Version: 2.4$
 * $Revision: 5$
 * $Step: 4$
 */

/****************************************************************************
 * 2.2 Data Types and Sizes
 * 2.2. Типы и размеры данных
 ****************************************************************************
 */
/* Упражнение 2.1. Напишите программу, которая будет выдавать диапазоны
 * значений типов char, short, int и long, описанных как signed и как
 * unsigned, с помощью печати соответствующих значений из стандартных
 * заголовочных файлов и путем прямого вычисления. Определите диапазоны чисел
 * с плавающей точкой различных типов. Вычислить эти диапазоны сложнее.
 * Упражнение 2.1. Напишите программу для определения диапазонов переменных
 * типов char, short, int, и long ( как signed, так и unsigned ) путем вывода
 * соответствующих значений из заголовочных файлов, а также с помощью
 * непосредственного вычисления.
 * Для второго способа усложним задачу: определите еще и диапазоны
 * вещественных типов.
 ****************************************************************************
 */
/* Exercise 2-1. Write a program to determine the ranges of char, short, int,
 * and long variables, both signed and unsigned, by printing appropriate
 * values from standard headers and by direct computation.
 * Harder if you compute them: determine the ranges of the various
 * floating-point types.
 ****************************************************************************
 */

#include <float.h>
#include <limits.h>
#include <stdio.h>

#define DEBUG                          1

#define FD_FLOAT_MAXLOOP               9999
#define FD_DOUBLE_MAXLOOP              99999

#if (defined(__BORLANDC__) && __BORLANDC__ == 0x410) \
    || (defined(_MSC_VER) && _MSC_VER <= 1600 && _MSC_VER >= 600) \
    || (defined(__WATCOMC__) && __WATCOMC__ >= 1200) \
    || __DJGPP__ || __DMC__
#define USE_DEFACEMENT                 1
#endif

#define USE_SIMPLE_CMP                 1
#if (defined(__WATCOMC__) && __WATCOMC__ >= 1250) \
    || (defined(_MSC_VER) && _MSC_VER <= 1200)
#undef USE_SIMPLE_CMP
#endif

#if !defined(LLONG_MIN) && defined(LONG_LONG_MIN)
#define LLONG_MIN                      LONG_LONG_MIN
#endif /* !defined(LLONG_MIN) && defined(LONG_LONG_MIN) */

#if !defined(LLONG_MAX) && defined(LONG_LONG_MAX)
#define LLONG_MAX                      LONG_LONG_MAX
#endif /* !defined(LLONG_MAX) && defined(LONG_LONG_MAX) */

#if !defined(ULLONG_MAX) && defined(ULONG_LONG_MAX)
#define ULLONG_MAX                     ULONG_LONG_MAX
#endif /* !defined(ULLONG_MAX) && defined(ULONG_LONG_MAX) */

#ifdef LLONG_MIN
#ifdef LLONG_MAX
#ifdef ULLONG_MAX
#define OK_LLONG                       1
#endif /* def ULLONG_MAX */
#endif /* def LLONG_MAX */
#endif /* def LLONG_MIN */

unsigned char uchar_max(void);
char schar_min(void);
char schar_max(void);

unsigned short ushort_max(void);
signed short short_min(void);
signed short short_max(void);

unsigned int uint_max(void);
signed int int_min(void);
signed int int_max(void);

unsigned long ulong_max(void);
signed long long_min(void);
signed long long_max(void);

int floats_are_equal(float x, float y);
int floats_almost_equal_zero(float x, float y);
float float_max(float base, float validity);
float float_min(float base, float validity);

int double_are_equal(double x, double y);
int double_almost_equal_zero(double x, double y);
double double_max(double base, double validity);
double double_min(double base, double validity);

int power_int(int base, int n);
float power_float(float base, int n);
double power_double(double base, int n);

float one_float = 1.0;
float zero_float = 0.0;

int main(void) {
    float max_float;
    float min_float;
    float min_stepf = 0.1;
    float nxt_stepf = 0.0;
    float eps_float;

    double max_double;
    double min_double;
    double min_step = 0.1;
    double nxt_stepd;
    double eps_double;

    int i, j;
    int k = 0;
    int e;

    printf("SCHAR_MIN             = %d\n", SCHAR_MIN);
    printf("schar_min()           = %d\n", schar_min());
    printf("SCHAR_MAX             = %d\n", SCHAR_MAX);
    printf("schar_max()           = %d\n", schar_max());
    printf("UCHAR_MAX             = %u\n", UCHAR_MAX);
    printf("uchar_max()           = %d\n", (unsigned char)uchar_max());
    printf("SHRT_MIN              = %d\n", SHRT_MIN);
    printf("short_min()           = %d\n", short_min());
    printf("SHRT_MAX              = %d\n", SHRT_MAX);
    printf("short_max()           = %d\n", short_max());
    printf("USHRT_MAX             = %u\n", USHRT_MAX);
    printf("ushort_max()          = %u\n", (unsigned short)ushort_max());
    printf("INT_MIN               = %d\n", INT_MIN);
    printf("int_min()             = %d\n", int_min());
    printf("INT_MAX               = %d\n", INT_MAX);
    printf("int_max()             = %d\n", int_max());
    printf("UINT_MAX              = %u\n", UINT_MAX);
    printf("uint_max()            = %u\n", (unsigned)uint_max());
    printf("LONG_MIN              = %ld\n", LONG_MIN);
    printf("long_min()            = %ld\n", long_min());
    printf("LONG_MAX              = %ld\n", LONG_MAX);
    printf("long_max()            = %ld\n", long_max());
    printf("ULONG_MAX             = %lu\n", ULONG_MAX);
    printf("ulong_max()           = %lu\n", (unsigned long)ulong_max());

#ifdef OK_LLONG
#if ( defined(__STDC__) && defined(__STDC_VERSION__) \
       && (__STDC__ && __STDC_VERSION__ >= 199901L) ) \
     || defined(__GNUC__) || defined(__MWERKS__) \
     || defined (__SUNPRO_C) || defined (__SUNPRO_CC) \
     || defined (__APPLE_CC__) || defined (_CRAYC) \
     || defined (_LONG_LONG)
    printf("LLONG_MIN             = %lld\n", LLONG_MIN);
    printf("LLONG_MAX             = %lld\n", LLONG_MAX);
    printf("ULLONG_MAX            = %llu\n", ULLONG_MAX);
#elif (defined (__BORLANDC__) && __BORLANDC__ > 0x460) \
       || (defined(__WATCOMC__) && defined(__WATCOM_INT64__))
    printf("LLONG_MIN             = %I64d\n", LLONG_MIN);
    printf("LLONG_MAX             = %I64d\n", LLONG_MAX);
    printf("ULLONG_MAX            = %I64u\n", ULLONG_MAX);
#endif /* if (defined(__STDC__) && defined(__STDC_VERSION__) ... */
    printf("\n");
#endif

#ifdef USE_DEFACEMENT
    printf("USE_DEFACEMENT: %d\n", USE_DEFACEMENT);
#else
    printf("USE_DEFACEMENT: NO\n");
#endif
#ifdef USE_SIMPLE_CMP
    printf("USE_SIMPLE_CMP: %d\n", USE_SIMPLE_CMP);
#else
    printf("USE_SIMPLE_CMP: NO\n");
#endif
    printf("FLT_MIN               = %e\n", FLT_MIN);
    printf("FLT_MAX               = %e\n", FLT_MAX);

#ifndef FLTBRK
    min_float = float_min(1.0, 2.0);
    printf("float_min             = %e\n", min_float);

    max_float = float_max(1.0, 2.0);
    printf("float_max[%d]          = %e\n", k++, max_float);
    e = FD_FLOAT_MAXLOOP + 1;
    for (j = 1; j < e; j++) {
        for (i = 1; i < e; i++) {
            nxt_stepf = min_stepf / i;
#ifdef USE_DEFACEMENT
            if (floats_are_equal(one_float, one_float + nxt_stepf)) {
#else
            if (0.0 == ((1 + nxt_stepf) - 1)) {
#endif
                i = e;
                j = e;
            }
            else {
                eps_float = nxt_stepf;
                max_float = float_max(max_float, one_float + eps_float);
                k++;
            }
        }
        min_stepf = eps_float;
    }
    printf("float_max[%07d]    = %e\n", k++, max_float);
#endif

    k = 0;
    printf("DBL_MIN               = %e\n", DBL_MIN);
    printf("DBL_MAX               = %e\n", DBL_MAX);

#ifndef FLTBRK
#ifndef DBLBRK
    min_double = double_min(1.0, 2.0);
    printf("double_min            = %le\n", min_double);
    max_double = double_max(1.0, 2.0);
    printf("double_max[%d]         = %le\n", k++, max_double);
    for (j = 1; j < e; j++) {
        for (i = 1; i < e; i++) {
            nxt_stepd = min_step / i;
#ifdef USE_DEFACEMENT
            if (double_are_equal(1.0, 1.0 + nxt_stepd)) {
#else
            if (0.0 == ((1 + nxt_stepd) - 1)) {
#endif
                i = e;
                j = e;
            }
            else {
                eps_double = nxt_stepd;
                max_double = double_max(max_double, (1 + eps_double));
                k++;
            }
        }
        min_step = eps_double;
    }
    printf("double_max[%07d]   = %le\n", k++, max_double);
#endif
#endif

    return 0;
}

unsigned char uchar_max(void) {
    unsigned char i = 0;
#ifdef DEBUG
    int c = 0;
#endif

    while (1) {
        i--;
#ifdef DEBUG
        c++;
        if (c > 1) {
            printf("schar_min: Error can't owerflow\n");
            printf("       c : %d ;  i : %d\n", c, i);
            return i;
        }
#endif
        if (0U < i) return i;
    }
}

char schar_min(void) {
    signed char r = 0;
    signed char i = uchar_max() / 2 + 1;

    while (1) {
        r = i;
        i--;
        if (0 <= i) return r;
    }
}

char schar_max(void) {
    signed char r = 0;
    signed char i = uchar_max() / 2;

    while (1) {
        r = i;
        i++;
        if (0 >= i) return r;
    }
}

unsigned short ushort_max(void) {
    unsigned short i = 0;

    while (1) {
        i--;
        if (0U < i) return i;
    }
}

signed short short_min(void) {
    signed short r = 0;
    signed short i = ushort_max() / 2 + 1;

    while (1) {
        r = i;
        i--;
        if (0 <= i) return r;
    }
}

signed short short_max(void) {
    signed short r = 0;
    signed short i = ushort_max() / 2;

    while (1) {
        r = i;
        i++;
        if (0 >= i) return r;
    }
}

unsigned int uint_max(void) {
    unsigned int i = 0;

    while (1) {
        i--;
        if (0U < i) return i;
    }
}

signed int int_min(void) {
    signed int r = 0;
    signed int i = uint_max() / 2 + 1;

    while (1) {
        r = i;
        i--;
        if (0 <= i) return r;
    }
}

signed int int_max(void) {
    signed int r = 0;
    signed int i = uint_max() / 2;

    while (1) {
        r = i;
        i++;
        if (0 >= i) return r;
    }
}

unsigned long ulong_max(void) {
    unsigned long i = 0;

    while (1) {
        i--;
        if (0UL < i) return i;
    }
}

signed long long_min(void) {
    signed long r = 0;
    signed long i = ulong_max() / 2 + 1;

    while (1) {
        r = i;
        i--;
        if (0L <= i) return r;
    }
}

signed long long_max(void) {
    signed long r = 0;
    signed long i = ulong_max() / 2;

    while (1) {
        r = i;
        i++;
        if (0L >= i) return r;
    }
}

int floats_are_equal(float x, float y) {
    return (x == y);
}

int floats_almost_equal_zero(float x, float y) {
    float z = 0.0;
    return !(x > y || y > x || x > z || x < z);
}

float float_max(float base, float validity) {
    int i;
    float b, max, t1, t2, x;

    b = base;
    max = base;
    for (i = 1; i < FD_FLOAT_MAXLOOP; i++) {
        b = b * validity;
        x = b / power_float(validity, i);
        t1 = base - x;
        t2 = x - base;
#ifdef USE_SIMPLE_CMP
        if (floats_are_equal(t1, t2)) {
#else
        if (floats_almost_equal_zero(t1, t2)){
#endif
            max = b;
        }
        else {
            return max;
        }
    }
    return max;
}

float float_min(float base, float validity) {
    int i;
    float b, min, t, t1, t2, x;

    b = base;
    for (i = 1; i < FD_FLOAT_MAXLOOP; i++) {
        t = b;
        b = b / validity;
        x = b * power_float(validity, i);
        t1 = base - x;
        t2 = x - base;
        if (t1 == t2) {
            min = t;
        }
        else {
            return min;
        }
    }
    return min;
}

int double_are_equal(double x, double y) {
    return (x == y);
}

int double_almost_equal_zero(double x, double y) {
    double z = 0.0;
    return !(x > y || y > x || x > z || x < z);
}

double double_max(double base, double validity) {
    long i;
    double b, max, t1, t2, x;

    b = base;
    max = base;
    for (i = 1; i < FD_DOUBLE_MAXLOOP; i++) {
        b = b * validity;
        x = b / power_double(validity, i);
        t1 = base - x;
        t2 = x - base;
#ifdef USE_SIMPLE_CMP
        if (double_are_equal(t1, t2)) {
#else
        if (double_almost_equal_zero(t1, t2)){
#endif
            max = b;
        }
        else {
            return max;
        }
    }
    return max;
}

double double_min(double base, double validity) {
    long i;
    double b, min, t, t1, t2, x;

    b = base;
    for (i = 1; i < FD_DOUBLE_MAXLOOP; i++) {
        t = b;
        b = b / validity;
        x = b * power_double(validity, i);
        t1 = base - x;
        t2 = x - base;
#ifdef USE_SIMPLE_CMP
        if (double_are_equal(t1, t2)) {
#else
        if (double_almost_equal_zero(t1, t2)){
#endif
            min = t;
        }
        else {
            return min;
        }
    }
    return min;
}

/* power: raise base to n-th power; n >= 0; version 2 */
int power_int(int base, int n)
{
    int p;

    for (p = 1; n > 0; --n)
        p = p * base;
    return p;
}

/* power: raise base to n-th power; n >= 0; version 2 */
float power_float(float base, int n)
{
    float p;

    for (p = 1; n > 0; --n)
        p = p * base;
    return p;
}

/* power: raise base to n-th power; n >= 0; version 2 */
double power_double(double base, int n)
{
    double p;

    for (p = 1; n > 0; --n)
        p = p * base;
    return p;
}

/* vim: syntax=c:paste:ff=unix:textwidth=78:ts=4:sw=4:sts=4:et
 * EOF */
