/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 0$
 */
/* count digits, white space, others */
/* 1.6 Arrays
 * A program to count the number of occurrences of each digit, of
 * white space characters (blank, tab, newline), and of all other
 * characters.
 * Программа для подсчета числа появлений каждой из цифр, символов
 * разделителей (пробел, табуляция, новая строка) и всех остальных
 * символов.
 */

#include <stdio.h>

main()
{
  int c, i, nwhite, nother;
  int ndigit[10];

  nwhite = nother = 0;
  for (i = 0; i < 10; ++i)
    ndigit[i] = 0;

  while ((c = getchar()) != EOF)
    if (c >= '0' && c <= '9')
      ++ndigit[c-'0'];
    else if (c == ' ' || c == '\n' || c == '\t')
      ++nwhite;
    else
      ++nother;

  printf("digits =");
  for (i = 0; i < 10; ++i)
    printf(" %d", ndigit[i]);
  printf(", white space = %d, other = %d\n", nwhite, nother);
}
