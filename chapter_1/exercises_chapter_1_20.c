/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* Exercise 1-20. Write a program detab that replaces tabs in the input with
 * the proper number of blanks to space to the next tab stop. Assume a fixed
 * set of tab stops, say every n columns.
 * Should n be a variable or a symbolic parameter? */
/* Упражнение 1.20. Напишите  программу  detab, которая  бы заменяла символы
 * табуляции во входном потоке соответствующим количеством пробелов до
 * следующей границы табуляции. Предположим, что табуляция имеет фиксированную
 * ширину n столбцов. Следует ли сделать n переменной или символическим
 * параметром?
*/

#include <stdio.h>

#define MAXLINE 255 /* maximum input line length */
#define EOL '\0'
#define NL '\n'
#define SP ' '
#define TAB '\t'

int tabsize = 8;                                         /* размер табуляции */
                                                /* длина максимальной строки */
int max = MAXLINE;                                         /* maximum length */
                                                           /* текущая строка */
char line[MAXLINE];                                    /* current input line */

void detab(char to[], char from[]);

int ckr_getline(char s[], int lim);
void copy(char to[], char from[]);
int chomp(char s[]);

int main() {
                                                    /* длина текущей строки */
  int len;                                          /* длина текущей строки */
  char in_line[MAXLINE];                        /* текущая введенная строка */
  char out_line[MAXLINE];               /* строка/массив символов/результат */

  while ((len = ckr_getline(in_line, MAXLINE)) > 0) {
    chomp(in_line);
    detab(out_line, in_line);
    puts(out_line);
  }

  return 0;
}

/* detab: copy 'from' into 'to' and replaces tabs in the to */
void detab(char to[], char from[]) {
  int j = 0, nt;

  /* основной цикл по строке */
  for (int i = 0; from[i] != EOL && i < max; ++i) {
    if (from[i] == TAB) {                            /* текущий символ TAB? */
      nt = (j / tabsize + 1) * tabsize;    /* расчитываем позицию табуляции */
      while (j < nt) {                   /* цикл замены табуляции пробелами */
        to[j++] = SP;
      }
    } else {
      to[j++] = from[i];
    }
  }
}

/* ckr_getline: read a line into s, return length */
/* ckr_getline: считывает строку в s, возвращает ее длину */
int ckr_getline(char s[], int lim) {
  int c, i;

  for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
    s[i] = c;
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';
  return i;
}

/* copy: copy 'from' into 'to'; assume to is big enough */
/* copy: копирует строку */
void copy(char to[], char from[]) {
  int i;

  i = 0;
  while ((to[i] = from[i]) != EOL)
    ++i;
}

/* chomp: removes any trailing string that corresponds to the current
 * value */
int chomp(char *s) {
  if (!s || !*s) return 0;
  while (s[1]) ++s;
    if (*s != NL) return 0;
  *s = 0;

  return NL;
}

/* vim: syntax=c:paste:ff=unix:textwidth=78:ts=2:sw=2
 * EOF */
