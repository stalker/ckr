/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */
/* Exercise 1-9. Write a program to copy its input to its output, 
 * replacing each string of one or more blanks by a single blank. */
/* Напишите программу, копирующую вводимые символы в выходной поток 
 * и заменяющую несколько стоящих подряд пробелов на один пробел.                            
 */

#include <stdio.h>
#define  SP ' '

main() {
  int c;                             /* переменная для текущего символа/знака */
  int sp = 0;                                     /* счётчик текущих пробелов */
  while ((c = getchar()) != EOF) {                         /* читаем до EOF-а */
    if (SP != c) {                                             /* не пробел ? */
      if (sp > 0) {                         /* до этого были пробел/пробелы ? */
        sp = 0;                                   /* текущий символ не пробел */
        putchar(SP);    /* собственно замена больше чем ноль пробелов на один */
      }
      putchar(c);
    } else {
      sp++;
      if (sp < 0 ) {  /* счётчик пробелов переполнился? здесь лучше проверка: */
        sp = 1;              /* INT_MAX == sp но мы про неё пока не знаем! :) */
      }
    }
  }
}
/* EOF */
