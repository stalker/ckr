/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* программа вывода строк длинее на stout */
/* Упражнение 1.17. Напишите программу для вывода всех строк входного 
 * потока, имеющих длину более 80 символов.
 * Exercise 1-17. Write a program to print all input lines that are longer 
 * than 80 characters.
 */

#include <stdio.h>

#define MAXLINE 1024 /* максимальная длина строки в потоке */
#define TESTLINE                                                               80
#define NL '\n'
#define EOL '\0'

int get_line(char line[], int maxline);
void copy(char to[], char from[]);
void put_s(char line[], int maxline);

/* вывод самой длинной строки в потоке */
int main() {
  int len; /* длина текущей строки */
  int max; /* текущая максимальная длина */
  char line[MAXLINE]; /* текущая введенная строка */
  char longest[MAXLINE]; /* самая длинная строка из введенных */
  int c;

  max = 0;
  while ((len = get_line(line, MAXLINE)) > 0) {
    printf("len: %d\n", len);
    /* если строка больше MAXLINE, продолжаем считаем количество символов */ 
    if (TESTLINE < len-1) {
      put_s(line, MAXLINE);
      if (line[len-1] != '\n') {
        while ((c = getchar()) != EOF && c != NL) {
          ++len;
          putchar(c);
        }
      }
      putchar('\n');
    }
  }

  return 0;
}

/* get_line: считывает строку в s, возвращает ее длину */
int get_line (char s[], int lim)
{
  int c, i;

  for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != NL && c != EOL; ++i)
    s[i] = c;

  if (c == NL) {
    s[i] = c;
    ++i;
  }
  s[i] = EOL;

  return i;
}

/* copy: копирует строку 'from' в 'to'; длина to считается достаточно точной */
void copy(char to[], char from[]) {
  int i = 0;

  while ((to[i] = from[i]) != EOL)
    ++i;
}

/* put_s: */
void put_s(char s[], int lim) {
  for 
    (int i = 0; i < lim - 1 && s[i] != EOF && s[i] != NL && s[i] != EOL; ++i)
    putchar(s[i]);
}

/* vim: syntax=c:paste:ff=unix:textwidth=78:ts=2:sw=2
 * EOF */
