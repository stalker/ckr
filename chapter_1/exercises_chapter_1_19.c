/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* Упражнение 1.19. Напишите функцию reverse(s), которая переписывает
 * свой строковый аргумент s в обратном порядке. Воспользуйтесь ею для
 * написания программы, которая бы выполняла такое обращение над каждой
 * строкой входного потока по очереди. */
/* Exercise 1-19. Write a function reverse(s) that reverses the character
 * string s. Use it to write a program that reverses its input a line at
 * a time.
 */

#include <stdio.h>

#define MAXLINE 255 /* максимальная длина строки в потоке */
#define EOL '\0'
#define NL '\n'
#define SP ' '
#define TAB '\t'

int get_line(char line[], int maxline);
void copy(char to[], char from[]);
void put_s(char line[], int maxline);
void space_to_zero(char line[], int maxline);
void reverse(char line[], int lenght);
void reverse2(char line[], int lenght);

/* вывод самой длинной строки в потоке */
int main() {
  int len; /* длина текущей строки */
  int max; /* текущая максимальная длина */
  char line[MAXLINE]; /* текущая введенная строка */
  char longest[MAXLINE]; /* самая длинная строка из введенных */
  int c;

  max = 0;
  while ((len = get_line(line, MAXLINE)) > 0) {
    printf("len: %d\n", len);
    reverse2(line, len);
  }

  return 0;
}

/* get_line: считывает строку в s, возвращает ее длину */
int get_line (char s[], int lim) {
  int c, i;

  for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != NL && c != EOL; ++i)
    s[i] = c;

  if (c == NL) {
    s[i] = c;
    ++i;
  }
  s[i] = EOL;

  return i;
}

/* copy: копирует строку 'from' в 'to'; длина to считается достаточно точной */
void copy(char to[], char from[]) {
  int i = 0;

  while ((to[i] = from[i]) != EOL)
    ++i;
}

/* put_s: */
void put_s(char s[], int lim) {
  for (int i = 0; i < lim - 1 && s[i] != EOF && s[i] != NL && s[i] != EOL; ++i)
    putchar(s[i]);
}

/* space_to_zero */
void space_to_zero(char s[], int lim) {
  int i;
  for (i = 0; i < lim - 1 && s[i] != NL && s[i] != EOL; ++i);
  for (int j = lim-1; j > i; --j)
    if (s[i] == SP || s[i] == TAB) s[i] = EOL;
    else j = i;
}

/* reverse version 1 */
void reverse(char s[], int len) {
  int i, j = len - 1;
  char t[MAXLINE];
  copy(t, s);
  t[j--] = NL;
  for (i = 0; i < len - 1 && s[i] != NL && s[i] != EOL; ++i)
    t[j--] = s[i];
  copy(s, t);
}


/* reverse version 2 */
void reverse2(char s[], int len) {
  int i, j = len - 1, m = (len - 1) / 2;
  char t;
  if (s[j] == NL || s[j] == EOL) {
    j = len - 2;
  }
  for (i = 0; i < m; ++i) {
    t = s[j];
    s[j--] = s[i];
    s[i] = t;
  }
}

/* vim: syntax=c:paste:ff=unix:textwidth=78:ts=2:sw=2
 * EOF */
