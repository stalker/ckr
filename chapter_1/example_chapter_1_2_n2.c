/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/*
 * The more serious problem is that because we have used integer arithmetic,
 * the Celsius temperatures are not very accurate; for instance, 0°F is actually
 * about -17.8°C, not -17. To get more accurate answers, we should use 
 * floating-point arithmetic instead of integer.
 */
/* Печать таблицы температур по Фаренгейту
 * и Цельсию для fahr = 0, 20, ..., 300.
 * Вариант с плавающей точкой.
 */

#include <stdio.h>

/* print Fahrenheit-Celsius table
 * for fahr = 0, 20, ..., 300; floating-point version */
main() {
  float fahr, celsius; float lower, upper, step;
  lower = 0; 	/* lower limit of temperatuire scale */
  upper = 300; 	/* upper limit */
  step = 20;	/* step size */
  fahr = lower;
  while (fahr <= upper) {
    celsius = (5.0/9.0) * (fahr-32.0);
    printf("%3.0f %6.1f\n", fahr, celsius);
    fahr = fahr + step;
  }
}
