/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* Exercise 1-12. Write a program that prints its input one word per line. */
/* Напишите программу, которая печатает вводимые 
 * данные, помещая по одному слову на строке.*/

#include <stdio.h>

#define IN 1 /* inside a word */
#define OUT 0 /* outside a word */

main()
{
  int c, state;
  while ((c = getchar()) != EOF) {
    if (c == ' ' || c == '\n' || c == '\t')
      c = '\n';
    putchar(c);
  }
}
/* EOF */
