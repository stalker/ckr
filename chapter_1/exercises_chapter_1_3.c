/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 2$
 */
/* print Fahrenheit-Celsius table
 * for fahr = 0, 20, ..., 300; floating-point version */
/* Усовершенствуйте программу преобразования температур таким образом, чтобы над
 * таблицей она печатала заголовок.
 */

#include <stdio.h>

main() {
  float fahr, celsius; float lower, upper, step;
  lower = 0; 	/* lower limit of temperatuire scale */
  upper = 300; 	/* upper limit */
  step = 20;	/* step size */
  fahr = lower;
  printf("%4s %7s\n", "fahr", "celsius");
  while (fahr <= upper) {
    celsius = (5.0/9.0) * (fahr-32.0);
    printf("%4.0f %6.1f\n", fahr, celsius);
    fahr = fahr + step;
  }
}
/* EOF */
