/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* 1.7 Functions
 *     Here is the function power and a main program to exercise it, so
 *     you can see the whole structure at once.
 * 1.7. Функции Итак, у нас есть функция power и использующая ее главная
 *      функция main, так что вся программа выглядит следующим образом:
 */

#include <stdio.h>

int power(int m, int n);

/* test power function */
/* Тест функции power */
int main()
{
  int i;
  for (i = 0; i < 10; ++i)
    printf("%d %d %d\n", i, power(2,i), power(-3,i));
  return 0;
}

/* power: raise base to n-th power; n >= 0 */
/* Возведение base в степень n, n >= 0 */
int power(int base, int n)
{
  int i, p;
  p = 1;
  for (i = 1; i <= n; ++i)
    p = p * base;
  return p;
}

/* vim: syntax=c:paste:ff=unix:textwidth=78:ts=2:sw=2
 * EOF */
