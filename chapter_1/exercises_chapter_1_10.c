/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* Exercise 1-10. Write a program to copy its input to its output, replacing 
 * each tab by \t, each backspace by \b, and each backslash by \\. This makes 
 * tabs and backspaces visible in an unambiguous way. */
/* copy input to output; 1st version */
/* Напишите программу, копирующую вводимые символы в выходной поток с заменой
 * символа табуляции на \t, символа забоя на \b и каждой обратной наклонной
 * черты на \\. Это сделает видимыми все символы табуляции и забоя_.
 */

#include <stdio.h>

#define BACKSP '\b'
#define TAB    '\t'
#define DBLBSL '\\'

main() {
  int c;
  c = getchar();
  while (c != EOF) {
    if (TAB == c) {
	putchar(DBLBSL);
	putchar('t');
    } else if (BACKSP == c) {
	putchar(DBLBSL);
	putchar('b');
    } else if (DBLBSL == c) {
	putchar(DBLBSL);
	putchar(DBLBSL);
    } else
	putchar(c);
    c = getchar();
  }
}
/* EOF */
