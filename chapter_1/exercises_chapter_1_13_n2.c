/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* Exercise 1-13. Write a program to print a histogram of the lengths
 * of words in its input.
 * It is easy to draw the histogram with the bars horizontal;
 * a vertical orientation is more challenging.
 * Упражнение 1-13 Напишите программу, печатающую гистограммы длин вводимых слов.
 * Гистограмму легко рисовать горизонтальными полосами. Рисование вертикальных
 * полос — более трудная задача.
 */

#include <stdio.h>

#define IN  1  /* эти символические константы введены в качестве индикатора,*/
#define OUT 0  /* "находимся ли мы в слове" или за его пределами            */

const int max_size = 70;                       /* максимальная длинна слова */
                  /* простая/медленная ф-ция нахождения максимума в векторе */
/* function: max_elem
 * a - array of int
 * size - size of array a
 * return maximum element of array a
 */
int max_elem(int *a, int size)
{
  int i;
  int max = a[0];
  for (i = 0 ; i < size ; i++)
    if (a[i]>max) max = a[i];
  return max;
}

          /* Напишите программу, печатающую гистограммы длин вводимых слов. */
int main() {
  int c;                           /* переменная для текущего символа/знака */
  int i, j;                                          /* счётчики для циклов */
  int nc = 0;                 /* счётчик для подсчёта длинны текущего слова */
  int max;
  int ndigit[max_size];                           /* массив для гистограммы */
  int state = OUT;                           /* статус "нахождения в слове" */

  for (i = 0; i < max_size; ++i)                   /* инициализируем ndigit */
     ndigit[i] = 0;

  while ((c = getchar()) != EOF) {                /* сновной цикл для ввода */
    if (c == ' ' || c == '\n' || c == '\t') { /* слова разделяются символами:
                                               * ' ' '\n' '\t'              */
      if (nc > (max_size - 1)) {      /* если перешагнули ограничение слова */
        nc = (max_size - 1);                /* длинна остаётся как максимум */
      }
      if(IN == state) {                                    /* были в слове? */
        ++ndigit[nc];                                     /* учли это слово */
      }
      state = OUT;                              /* перешли за границу слова */
      nc = 0;                      /* обнулили счётчик для следующего слова */
    }
    else {
      state = IN;                                                /* в слове */
      ++nc;                                               /* считаем длинну */
    }
  }
  max = max_elem(ndigit, max_size);
  printf("max = %d\n", max);
                                                      /* рисуем гистограмму */
  for (i = max; i > 0; i--) {
    for (j = 1; j < max_size; j++) {
      if (ndigit[j] >= i)
        putchar('X');
      else
        putchar(' ');
    }
    putchar('\n');
  }
  for (j = 1; j < max_size; j++) {
    putchar('^');
  }
  putchar('\n');
  for (j = 1; j < max_size; j++) {
    i = (j / 10) % 10;
    if (0 != i) {
      printf("%d", (j/10)%10);
    } else {
      putchar(' ');
    }
  }
  putchar('\n');
  for (j = 1; j < max_size; j++) {
    printf("%d", j % 10);
  }
  putchar('\n');
}
/* EOF */
