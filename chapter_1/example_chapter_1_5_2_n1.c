/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* count characters in input; 1st version */ 
/* 1.5.2. Подсчет символов
 * Следующая программа похожа на программу копирования, но занимается подсчетом
 * символов.
 */

#include <stdio.h>

main()
{
  long nc;
  nc = 0;
  while (getchar() != EOF)
    ++nc;
  printf("%ld\n", nc);
}
