/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* Exercise 1-7. Write a program to print the value of EOF. */
/* Напишите программу, печатающую значение константы EOF. */

#include <stdio.h>

main() {
  printf("EOF = oct:0%o dec:%d hex:0x%X\n", EOF, EOF, EOF); 
}
/* EOF */
