/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 2$
 */
/* count lines, words, and characters in input */
/* Напишите программу, которая печатает вводимые 
 * данные, помещая по одному слову на строке.*/

#include <stdio.h>

#define IN 1 /* inside a word */
#define OUT 0 /* outside a word */

main()
{
  int c, state;
  while ((c = getchar()) != EOF) {
    if (c == ' ' || c == '\n' || c == '\t')
      c = '\n';
    putchar(c);
  }
}
/* EOF */
