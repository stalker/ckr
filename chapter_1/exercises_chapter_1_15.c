/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 0$
 */
/* 1.7 Functions
 * 1.7 Функции
 */
/* Exercise 1.15. Rewrite the temperature conversion program of Section 1.2 
 * to use a function for conversion. */
/* Упражнение 1.15. Перепишите программу преобразования температур из
 * раздела 1.2 так, чтобы само преобразование выполнялось функцией.
 */

#include <stdio.h>

double celsius_to_fahr(double celsius);

int main() {
  double fahr, celsius;                /* температура по Фарингейту и Цельсию */
  double lower, upper, step;
  lower = -10;                         /*           нижняя граница температур */
  upper = 300;                         /*          верхняя граница температур */
  step = 10;                           /*                       величина шага */
  celsius = lower;    /*  задаем начальное значение температуры по Фаренгейту */
  printf("%6s %4s\n", "celsius", "fahr");                /* выводим заголовок */
  while (celsius <= upper) {
    fahr = celsius_to_fahr(celsius);
    printf("%6.1f  %5.1f\n", celsius, fahr);
    celsius = celsius + step;
  }
  return 0;
}

/* функция преобразования температур из значения по шкале Цельсия
 * в значение по Фарингейту */
double celsius_to_fahr(double celsius) {
  return (9.0*celsius + 160.0) / 5.0;
}

/* vim: syntax=c:paste:ff=unix:textwidth=78:ts=2:sw=2
 * EOF */
