/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* count lines in input */
/* Следующая программа подсчитывает строки.
 */

#include <stdio.h>

main()
{
  int c, nl;
  nl = 0;
  while ((c = getchar()) != EOF)
    if (c == '\n')
      ++nl;
  printf("%d\n", nl);
}
