/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* Exercise 1-8. Write a program to count blanks, tabs, and newlines. */
/* Напишите программу для подсчета пробелов, табуляций и строк. */

#include <stdio.h>

main() {
  int c, nl = 0, sp = 0, tb = 0;
  while ((c = getchar()) != EOF) {
    if (c == '\n')
	++nl;
    if (c == ' ')
	++sp;
    if (c == '\t')
	++tb;
  }
  printf("newline: %d, space = %d, tabs = %d\n", nl, sp, tb);
}
/* EOF */
