/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* Exercise 1-20. Write a program detab that replaces tabs in the input
 * with the proper number of blanks to space to the next tab stop.
 * Assume a fixed set of tab stops, say every n columns.
 * Should n be a variable or a symbolic parameter? */
/* Упражнение 1.21. Напишите программу  entab, которая бы заменяла пустые
 * строки, состоящие   из  одних пробелов, строками, содержащими  минимальное
 * количество табуляций и дополнительных пробелов, — так, чтобы заполнять то
 * же пространство.
 * Используйте  те  же  параметры  табуляции, что  и в программе detab.
 * Если для заполнения места до следующей границы табуляции требуется один
 * пробел или один символ табуляции, то что следует предпочесть?
 */

#include <stdio.h>

#define MAXLINE 255                   /* максимальная длина строки в потоке */
#define MAX_UI  255                   /* максимальная длина строки в потоке */

enum boolean { NO, YES };
enum escapes { BACKSPACE = '\b', NEWLINE = '\n', RETURN = '\r' };
enum escs    { EOL = '\0', BELL = '\a', BS = '\b', TAB = '\t',
               NL = '\n', VTAB = '\v', RETN = '\r', SP = ' ' };
enum months  { JAN = 1, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV,
               DEC };

int tabsize = 8;

void entab(char to[], char from[]);
void detab(char to[], char from[]);

int ckr_getline(char s[], int lim);
unsigned int ui_chomp(char *s);

/* print the longest input line */
int main() {
  int len;		                                      /* длина текущей строки */
  char in_line[MAXLINE];                        /* текущая введенная строка */
  char out_line[MAXLINE];               /* строка/массив символов/результат */

  while ((len = ckr_getline(in_line, MAXLINE)) > 0) {
    ui_chomp(in_line);
    puts(in_line);
    entab(out_line, in_line);
    puts(out_line);
  }

  return 0;
}

/* entab: copy 'from' into 'to' and replaces space in the to */
void entab(char to[], char from[]) {
  int i, j = 0, t = 0, nt;

  for (i = 0; from[i] != EOL && i < MAXLINE;) {
    if (from[i] == SP) {
      int k = j;
      nt = (t / tabsize + 1) * tabsize;
      while (from[i] == SP && i < nt) { i++; t++; k++; }
      if (t == nt) {
        to[j++] = TAB;
      } else {
        while(j < k) {
          to[j++] = SP;
        }
      }
      i--;
      t--;
    } else if (from[i] == TAB) {
      nt = (t / tabsize + 1) * tabsize;
      t = nt - 1;
      to[j++] = from[i];
    } else {
      to[j++] = from[i];
    }
    i++;
    t++;
  }
  to[j] = 0;
}

/* ckr_getline: read a line into s, return length */
/* ckr_getline: считывает строку в s, возвращает ее длину */
int ckr_getline(char s[], int lim) {
  int c, i;

  for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
    s[i] = c;
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';
  return i;
}

/* ui_chomp: removes any trailing string that corresponds to the current
 * value */
unsigned int ui_chomp(char *s) {
  unsigned int i;

  if (!s || !*s) { return 0; }

  while (s[1]) {
    if (MAX_UI < i) {
      *s = EOL;
    } else {
      ++i; ++s;
    }
  }

  if (*s != NL) { return 0; }
  *s = 0;

  return NL;
}
/* vim: syntax=c:paste:ff=unix:textwidth=78:ts=2:sw=2
 * EOF */
