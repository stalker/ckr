/* $Date$
 * $Id$
 * $Version: 0.7$
 * $Revision: 6$
 */
/* 1.10 External Variables and Scope                                          *
 * 1.10. Внешние переменные и область видимости                               *
 */
/* Exercise 1-24. Write a program to check a C program for rudimentary        *
 * syntax errors like unmatched parentheses, brackets and braces.             *
 * Don't forget about quotes, both single and double, escape sequences,       *
 * and comments. (This program is hard if you do it in full generality.)      *
 */
/* Упражнение 1.24. Напишите программу, проверяющую Си-программы на           *
 * элементарные синтаксические ошибки вроде несбалансированности скобок       *
 * всех видов. Не забудьте о кавычках (одиночных и двойных),                  *
 * эскейп-последовательностях (\...) и комментариях.                          *
 * (Это сложная программа, если писать ее для общего случая.)                 *
 */

#include <stdio.h>
                                                /* maximum input line length */
#define MAXLINE 255                    /* максимальная длина вводимой строки */
                                                      /* maximum body length */
#define MAXBODY 65535                           /* максимальный размер файла */

enum boolean { NO, YES };
enum escapes { BACKSPACE = '\b', NEWLINE = '\n', RETURN = '\r' };
enum escs    { EOL = '\0', BELL = '\a', BS = '\b', TAB = '\t',
               NL = '\n', VTAB = '\v', RETN = '\r', SP = ' ' };
enum months  { JAN = 1, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV,
               DEC };

/* Curly brackets or braces { } -- Фигурные скобки */
unsigned braces_left = 0;              /*   Счётчик фигурных скобок левых  { */
unsigned braces_right = 0;             /*   Счётчик фигурных скобок правых } */
/* Brackets or Square brackets [ ] -- Квадратные скобки */
unsigned brackets_left = 0;            /* Счётчик квадратных скобок левых  [ */
unsigned brackets_right = 0;           /* Счётчик квадратных скобок правых ] */
/* Parentheses  ( ) -- Квадратные скобки */
unsigned parentheses_left = 0;         /*    Счётчик круглых скобок левых  ( */
unsigned parentheses_right = 0;        /*    Счётчик круглых скобок правых ) */

unsigned len_cdefine = 0;
unsigned len_delimit = 0;

/*                                                                            *
 * tr_comm: триггер1 0 если "мы в комментарии"                                *
 *             1 если "мы не в комментарии"                                   *
 * tr_stng: триггер2 0 если "мы в строке ограниченной символом(") "           *
 *             1 если "мы не в строке"                                        *
 * tr_sdtc: триггер3 0 если "мы в строке ограниченной символом(') "           *
 *             1 если "мы не в строке"                                        *
*/
int tr_comm = 0;
int tr_stng = 0;
int tr_sdtc = 0;

const char cdefine[] = "#define";
const char delimit[] = { TAB, NL, 32, 33, 37, 40, 41, 42, 43, 44, 45, 47, 58,
                         59, 60, 61, 62, 63, 91, 93, 94, 123, 124, 125, 126, 0};

unsigned get_define(char def[], char body[], unsigned bi);
unsigned is_define(char body[], unsigned bi, char define[]);
unsigned is_string(char body[], unsigned bi, char define[]);
void substitute_define(char body[]);
int inside_iteration(char res[],char body[], char d[], char v[], unsigned bi);

int verification_of_compliance(char b[]);

int is_end_comm(char s[], unsigned i);
int is_double_backslash_in_str(char s[], unsigned i);
int is_double_qoutmark_in_str(char s[], unsigned i);
int is_single_qoutmark_in_stc(char s[], unsigned i);
int is_end_str(char s[], unsigned i);
int is_end_stc(char s[], unsigned i);
int is_start_comm(char s[], unsigned i);
int is_start_string(char s[], unsigned i);
int is_start_strconst(char s[], unsigned i);
int is_str_strconst_comm(void);
void copy_with_remove_comment(char t[], char s[]);

#define MAX_STACK 255
unsigned stack_push(char s, unsigned l, unsigned c);
unsigned stack_pointer(void);
char stack_top(void);
unsigned stack_top_line(void);
unsigned stack_top_col(void);
char stack_pop(void);
#define stack_top_l stack_top_line
#define stack_top_c stack_top_col

int ckr_getline(char s[], int lim);
int ckr_strlen(char s[]);

char out_body[MAXBODY];                         /* массив символов/результат */

/* let's go */
int main() {
  char sym;
  unsigned i = 0;
  unsigned len;                 /*current line length / длина текущей строки */
  char in_line[MAXLINE];                         /* текущая введенная строка */

  len_cdefine = ckr_strlen((char *)cdefine);
  len_delimit = ckr_strlen((char *)delimit);
  tr_comm = tr_stng = tr_sdtc = 0;

  /* основной_цикл, читаем STDIN */
  while ((len = ckr_getline(in_line, MAXLINE)) > 0) {
    i++; /* номер строки */
    /* копируем во временный буфер и удаляем комментарии */
    copy_with_remove_comment(out_body, in_line);
  }
  substitute_define(out_body);
  puts(out_body);

  /* производим проверку по нашему заданию */
  return verification_of_compliance(out_body);
}

/* ф-ция: get_define                                                          *
 * пар-ры:                                                                    *
 * d            -- массив символов для определения и его значения ,           *
 *                 заполняется в этой ф-ции                                   *
 * b            -- тело входного потока                                       *
 * k            -- текущая позиция в потоке                                   *
 * глобальные переменные:                                                     *
 * НЕТ                                                                        *
 * возвращает длину определения и значение в а так же возвращает определения  *
 * и зачтение пер-ной d, иначе возвращает 0(false)                            *
 *                                                                            *
 */
unsigned get_define(char d[], char b[], unsigned k) {
  unsigned i = 0;
  unsigned j = 0;

  while (i < len_cdefine) {
    if (b[k++] != cdefine[i++]) {
      return NO;
    }
  }
  while (SP == b[k] || TAB == b[k]) { ++k; }
  while (NL != b[k] && EOL != b[k]) {
    d[j++] = b[k++];
  }
  d[j] = EOL;

  return i + j;
}

/* ф-ция: get_value                                                           *
 * пар-ры:                                                                    *
 * v            -- массив символов для значения, заполняется в этой ф-ции     *
 *                 если значение отсутствует то v[0] = EOL                    *
 * d            -- строка с именем определения, с пустым или со значением     *
 * глобальные переменные:                                                     *
 * НЕТ                                                                        *
 * возвращает значение в пер-ной v если оно есть                              *
 * возвращает длину строки d                                                  *
 */
unsigned get_value(char v[], char d[]) {
  unsigned i = 0;
  unsigned j = 0;

  unsigned l = ckr_strlen(d);

  while (i < l && (SP != d[i] && TAB != d[i])) {
    if (RETN == d[i]) {
      d[i] = EOL;
      v[j] = EOL;
      return i;
    }
    i++;
  }

  while (i < l && (SP == d[i] || TAB == d[i])) { ++i; }
  d[i-1] = EOL;
  while (i < l && EOL != d[i] && NL != d[i]) {
    v[j++] = d[i++];
  }
  v[j] = EOL;

  return i;
}

/* ф-ция: is_delimiter                                                        *
 * пар-ры:                                                                    *
 * c            -- символ                                                     *
 * глобальные переменные:                                                     *
 * НЕТ                                                                        *
 * возвращает 1(true) если символ является членом массива delimit             *
 * иначе возвращает 0                                                         *
 */
unsigned is_delimiter(char c) {
  int i;
  for (i = 0; i < len_delimit; i++) {
    if (delimit[i] == c) {
      return YES;
    }
  }

  return NO;
}

/* ф-ция: is_define                                                           *
 * пар-ры:                                                                    *
 * b            -- текущая строка                                             *
 * k            -- позиция в текущей строке                                   *
 * d            -- строка определения                                         *
 * глобальные переменные:                                                     *
 * НЕТ                                                                        *
 * возвращает ??????? если найдена подстановка определения с позиции k        *
 * иначе возвращает 0                                                         *
 */
unsigned is_define(char b[], unsigned k, char d[]) {
  unsigned i = 0;
  unsigned s = 0;
  unsigned l = ckr_strlen(d);

  if (0 == (s = k)) return 0;

  while (i < l) {
    if (b[k++] != d[i++]) {
      return NO;
    }
  }

  if (is_delimiter(b[s-1]) && is_delimiter(b[k])) {
    return k - s;
  } else {
    return NO;
  }
}

char temp[MAXBODY];                             /* временный массив символов */

/* ф-ция: substitute_define                                                   *
 * подстановка макроопределений во входном потоке body                        *
 * результат сохраняется в temp                                               *
 */
void substitute_define(char body[]) {
  int i;
  unsigned bi = 0;
  unsigned len = 0;
  char define[MAXLINE];
  char value[MAXLINE];
  int old_tr_comm = 0;
  int old_tr_stng = 0;
  int old_tr_sdtc = 0;

  while (EOL != body[bi]) {
    if (is_double_backslash_in_str(body, bi)      /*   триггер2 или триггер3 */
                                          /* установлены и найден паттерн \\ */
        || is_double_qoutmark_in_str(body, bi)    /* ИЛИ триггер2 установлен */
                                                  /*     и найден паттерн \" */
        || is_single_qoutmark_in_stc(body, bi)    /* ИЛИ триггер3 установлен */
    ) {                                           /*     и найден паттерн \' */
      bi = bi + 2;
    } else if (is_end_str(body, bi)               /*     триггер2 установлен */
                                                  /*       и найден символ " */
               || is_end_stc(body, bi)            /*     триггер3 установлен */
                                                  /*       и найден символ ' */
               || is_start_string(body, bi)       /*        не в комментарии */
                                /*  и не установлен триггер3 и начало строки */
               || is_start_strconst(body, bi)     /*        не в комментарии */
                   /* и не установлен триггер2 и начало символьной константы */
               || is_str_strconst_comm()
    ) {
      bi++;
    } else if ((len = get_define(define, body, bi)) > 0) {
      get_value(value, define);   /* на текущей строке определение '#define' */
      bi = bi + len + 1;    /* в переменную value заносим определение define */
      old_tr_comm = tr_comm;         /* запомнить текущие значения триггеров */
      old_tr_stng = tr_stng;
      old_tr_sdtc = tr_sdtc;
      /*    с текущей позиции произвести подстановку определения define      */
      inside_iteration(temp, body, define, value, bi);
      tr_comm = old_tr_comm;              /* восстановить значения триггеров */
      tr_stng = old_tr_stng;
      tr_sdtc = old_tr_sdtc;
      bi = bi - len - 1; /* ??? - 1 */
      for (i = 0; temp[i] != EOL; i++) {
        body[bi++] = temp[i];
      }
      body[bi] = EOL;
    } else {
      /* Считаем !-) */
      bi++;
    }
  }

  return;
}

/* ф-ция: inside_iteration
 * пар-ры:
 * b            -- тело входного потока
 * d            -- имя макроопределения
 * v            -- значение макроопределения
 * bi           -- позиция во входном потоке
 * глобальные переменные:
 * НЕТ
 * возвращает: res ???
 */
int inside_iteration(char res[], char b[], char d[], char v[], unsigned bi) {
  int i;
  unsigned ti = 0;
  unsigned skip = 0;

  int len_v = ckr_strlen(v);
  tr_comm = tr_stng = tr_sdtc = 0;

  for (i = bi; b[i] != EOL;) {
    if (is_double_backslash_in_str(b, i)  /* триггер2 или триггер3 установлен /
                                           * и найден паттерн \\             */
        || is_double_qoutmark_in_str(b, i)/* ИЛИ триггер2 установлен          /
                                           *     и найден паттерн \"         */
        || is_single_qoutmark_in_stc(b, i)/* ИЛИ триггер3 установлен         */
    ) {                                   /*     и найден паттерн \'         */
      res[ti++] = b[i++];
      res[ti++] = b[i++];
    } else if (is_end_str(b, i)     /* триггер2 установлен и найден символ " */
               || is_end_stc(b, i)  /* триггер3 установлен и найден символ ' */
               || is_start_string(b, i)   /* не в комментарии и не установлен /
                                           * триггер3 и начало строки        */
               || is_start_strconst(b, i) /* не в комментарии и не установлен*/
    ) {                            /* триггер2 и начало символьной константы */
      res[ti++] = b[i++];
    }
    else if (!is_str_strconst_comm() && (skip = is_define(b, i, d)) > 0)
    {
      int j;
      for (j = 0; j < len_v; res[ti++] = v[j++]) { }
      i = i + skip;
    } else {
      res[ti++] = b[i++];
    }
  }
  res[ti] = EOL;

  return ti - bi;
}

/* ф-ция: verification_of_compliance                                          *
 * проверка на элементарные синтаксические ошибки вроде несбалансированности  *
 * скобок всех видов.                                                         *
 */
int verification_of_compliance(char b[]) {
  int error = NO;
  unsigned c = 1;
  unsigned i = 0;
  unsigned l = 1;
  /* цикл_до_конца_строки1 обрабатываем строку b её индекс i */
  while ( b[i] != EOL) {
    if (is_double_backslash_in_str(b, i)    /*установлен триггер2 или триггер3
                                             * и найден паттерн \\           */
        || is_double_qoutmark_in_str(b, i)  /* ИЛИ триггер2 установлен       */
                                            /*     и найден паттерн \"       */
        || is_single_qoutmark_in_stc(b, i) )/* ИЛИ триггер3 установлен       */
    {                                       /*     и найден паттерн \'       */
      i = i + 2; c = c + 2;
    } else if (is_end_str(b, i)     /* триггер2 установлен и найден символ " */
               || is_end_stc(b, i)  /* триггер3 установлен и найден символ ' */
               || is_start_string(b, i)   /* не в комментарии и не установлен /
                                           * триггер3 и начало строки        */
               || is_start_strconst(b, i) /* не в комментарии и не установлен /
                                      триггер2 и начало символьной константы */
               || is_str_strconst_comm() )
    {
      if (NL == b[i]) { c = 1; l++; } else { c++; }
      i++;
    } else {
      /* Считаем!!! */
      if ('{' == b[i]) {                      /*  текущий символ braces_left */
        braces_left++;
        stack_push(b[i], l, c);               /*              заносим в стек */
        i++; c++;                             /* перемещаем указатель дальше */
      } else if ('}' == b[i]) {               /*         найден braces_right */
        braces_right++;
        if ('{' == stack_top() && 0 < stack_top()) {
          /*      в стеке для текущего braces_right есть пара braces_left    */
          stack_pop();
        } else if (0 < stack_top()) {
          /*   в стеке для текущего braces_right нет пары. Фиксируем ошибку  */
          printf("Error pair for %c on line %d, col %d\n",
                 stack_top(), stack_top_l(), stack_top_c());
          error = YES;
        } else {
          /* ошибка и стек пуст                                              */
          printf("Error pair for %c on line %d, col %d\n", '}', l, c);
          error = YES;
        }
        i++; c++;
      } else if ('[' == b[i]) {              /* текущий символ brackets_left */
        brackets_left++;
        stack_push(b[i], l, c);              /*               заносим в стек */
        i++; c++;                            /*  перемещаем указатель дальше */
      } else if (']' == b[i]) {              /*        найден brackets_right */
        brackets_right++;
        if ('[' == stack_top() && 0 < stack_top()) {
          /*     в стеке для текущего brackets_right есть пара braces_left   */
          stack_pop();
        } else if (0 < stack_top()) {
          /*  в стеке для текущего brackets_right нет пары. Фиксируем ошибку */
          printf("Error pair for %c on line %d, col %d\n",
                 stack_top(), stack_top_l(), stack_top_c());
          error = YES;
        } else {
          /* ошибка и стек пуст                                              */
          printf("Error pair for %c on line %d, col %d\n", ']', l, c);
          error = YES;
        }
        i++; c++;
      } else if ('(' == b[i]) {           /* текущий символ parentheses_left */
        parentheses_left++;
        stack_push(b[i], l, c);           /*                  заносим в стек */
        i++; c++;                         /*     перемещаем указатель дальше */
      } else if (')' == b[i]) {           /*        найден parentheses_right */
        parentheses_right++;
        if ('(' == stack_top() && 0 < stack_top()) {
          /*   в стеке для текущего parentheses_right есть пара braces_left  */
          stack_pop();
        } else if (0 < stack_top()) {
          /*в стеке для текущего parentheses_right нет пары. Фиксируем ошибку*/
          printf("Error pair for %c on line %d, col %d\n",
                 stack_top(), stack_top_l(), stack_top_c());
          error = YES;
        } else {
          /* ошибка и стек пуст                                              */
          printf("Error pair for %c on line %d, col %d\n", ')', l, c);
          error = YES;
        }
        i++; c++;
      } else {
        /*         передвигаем указатель далее, учитываем переводы строк для */
        if (NL == b[i]) { c = 1; l++; } else { c++; }        /* отслеживания */
        i++;                                                 /*    координат */
      }
    }
  }
  if (0 < stack_pointer()) {
    printf("Error pair for %c on line %d, col %d\n",
           stack_top(), stack_top_l(), stack_top_c());
  }

  if (braces_left > braces_right) {
    printf("braces_left : %d\n", braces_left );
    printf("braces_right: %d\n", braces_right );
    return 1;
  } else if (braces_left < braces_right) {
    printf("braces_left : %d\n", braces_left );
    printf("braces_right: %d\n", braces_right );
    return 2;
  } else if (brackets_left > brackets_right) {
    printf("brackets_left : %d\n", brackets_left );
    printf("brackets_right: %d\n", brackets_right );
    return 3;
  } else if (brackets_left < brackets_right) {
    printf("brackets_left : %d\n", brackets_left );
    printf("brackets_right: %d\n", brackets_right );
    return 4;
  } else if (parentheses_left > parentheses_right) {
    printf("parentheses_left : %d\n", parentheses_left );
    printf("parentheses_right: %d\n", parentheses_right );
    return 5;
  } else if (parentheses_left < parentheses_right) {
    printf("parentheses_left : %d\n", parentheses_left );
    printf("parentheses_right: %d\n", parentheses_right );
    return 6;
  }

  return 0;
}

unsigned int bi = 0;

/* ф-ция: is_end_comm                                                         *
 * пар-ры:                                                                    *
 * s            -- текущая строка                                             *
 * i            -- позиция в текущей строке                                   *
 * глобальная переменная:                                                     *
 * tr_comm      -- триггер1 - признак нахождения в комментарии                *
 * tr_stng      -- триггер2 - признак нахождения в строке                     *
 * tr_sdtc      -- триггер3 - признак нахождения в символьной константе       *
 * возвращает 1(true) если найден конец комментария                           *
 * и устанавливает триггер1 - tr_comm в 0(false)                              *
 * иначе возвращает 0                                                         *
 */
int is_end_comm(char s[], unsigned i) {
  if (tr_comm && '*' == s[i] && '/' == s[i+1]) {
    tr_comm = NO;
    return YES;
  }
  return NO;
}

/* ф-ция: is_double_backslash_in_str                                          *
 * пар-ры:                                                                    *
 * s            -- текущая строка                                             *
 * i            -- позиция в текущей строке                                   *
 * глобальные переменные:                                                     *
 * tr_comm      -- триггер1 - признак нахождения в комментарии                *
 * tr_stng      -- триггер2 - признак нахождения в строке                     *
 * tr_sdtc      -- триггер3 - признак нахождения в символьной константе       *
 * возвращает 1(true) если найден паттерн \\ в строке или                     *
 * символьной константе                                                       *
 * иначе возвращает 0                                                         *
 */
int is_double_backslash_in_str(char s[], unsigned i) {
  return !tr_comm && (tr_stng || tr_sdtc) && '\\' == s[i] && '\\' == s[i+1];
}

/* ф-ция: is_double_qoutmark_in_str                                           *
 * пар-ры:                                                                    *
 * s            -- текущая строка                                             *
 * i            -- позиция в текущей строке                                   *
 * глобальные переменные:                                                     *
 * tr_comm      -- триггер1 - признак нахождения в комментарии                *
 * tr_stng      -- триггер2 - признак нахождения в строке                     *
 * возвращает 1(true) если найден паттерн \" в строке                         *
 * иначе возвращает 0
 */
int is_double_qoutmark_in_str(char s[], unsigned i) {
  return !tr_comm && tr_stng && '\\' == s[i] && '\"' != s[i+1];
}

/* ф-ция: is_single_qoutmark_in_stc                                           *
 * пар-ры:                                                                    *
 * s            -- текущая строка                                             *
 * i            -- позиция в текущей строке                                   *
 * глобальные переменные:                                                     *
 * tr_comm      -- триггер1 - признак нахождения в комментарии                *
 * tr_sdtc      -- триггер3 - признак нахождения в символьной константе       *
 * возвращает 1(true) если найден паттерн \' в символьной                     *
 * константе                                                                  *
 * иначе возвращает 0                                                         *
 */
int is_single_qoutmark_in_stc(char s[], unsigned i) {
  return !tr_comm && tr_sdtc && '\\' == s[i] && '\'' != s[i+1];
}

/* ф-ция: is_end_str                                                          *
 * пар-ры:                                                                    *
 * s            -- текущая строка                                             *
 * i            -- позиция в текущей строке                                   *
 * глобальные переменные:                                                     *
 * tr_comm      -- триггер1 - признак нахождения в комментарии                *
 * tr_stng      -- триггер2 - признак нахождения в строке                     *
 * возвращает 1(true) если найден конец строки                                *
 * и устанавливает триггер2 - tr_stng в 0(false)                              *
 * иначе возвращает 0                                                         *
 */
int is_end_str(char s[], unsigned i) {
  if (!tr_comm && tr_stng && '\"' == s[i]) {
    tr_stng = NO;
    return YES;
  }
  return NO;
}

/* ф-ция: is_end_stc                                                          *
 * пар-ры:                                                                    *
 * s            -- текущая строка                                             *
 * i            -- позиция в текущей строке                                   *
 * глобальные переменные:                                                     *
 * tr_comm      -- триггер1 - признак нахождения в комментарии                *
 * tr_stng      -- триггер2 - признак нахождения в строке                     *
 * возвращает 1(true) если найден конец символьной                            *
 * константы и устанавливает триггер2 - tr_sdtc в 0(false)                    *
 * иначе возвращает 0                                                         *
 */
int is_end_stc(char s[], unsigned i) {
  if (!tr_comm && tr_sdtc && '\'' == s[i]) {
    tr_sdtc = NO;
    return YES;
  }
  return NO;
}

/* ф-ция: is_start_comm                                                       *
 * пар-ры:                                                                    *
 * s            -- текущая строка                                             *
 * i            -- позиция в текущей строке                                   *
 * глобальные переменные:                                                     *
 * tr_comm      -- триггер1 - признак нахождения в комментарии                *
 * tr_stng      -- триггер2 - признак нахождения в строке                     *
 * tr_sdtc      -- триггер3 - признак нахождения в символьной константе       *
 * возвращает 1(true) если найдено начало комментария                         *
 * и устанавливает триггер1 - tr_comm в 1(true)                               *
 * иначе возвращает 0                                                         *
 */
int is_start_comm(char s[], unsigned i) {
  if (!(tr_comm || tr_stng || tr_sdtc) && '/' == s[i] && '*' == s[i+1]) {
    tr_comm = YES;
    return YES;
  }
  return NO;
}

/* ф-ция: is_start_string                                                     *
 * пар-ры:                                                                    *
 * s            -- текущая строка                                             *
 * i            -- позиция в текущей строке                                   *
 * глобальные переменные:                                                     *
 * tr_comm      -- триггер1 - признак нахождения в комментарии                *
 * tr_stng      -- триггер2 - признак нахождения в строке                     *
 * возвращает 1(true) если найден начало строки                               *
 * и устанавливает триггер1 - tr_stng в 1(true)                               *
 * иначе возвращает 0                                                         *
 */
int is_start_string(char s[], unsigned i) {
  if (!(tr_comm || tr_sdtc) && '"' == s[i]) {
    tr_stng = YES;
    return YES;
  }
  return NO;
}

/* ф-ция: is_start_string                                                     *
 * пар-ры:                                                                    *
 * s            -- текущая строка                                             *
 * i            -- позиция в текущей строке                                   *
 * глобальные переменные:                                                     *
 * tr_comm      -- триггер1 - признак нахождения в комментарии                *
 * tr_stng      -- триггер2 - признак нахождения в строке                     *
 * возвращает 1(true) если найден начало строки                               *
 * и устанавливает триггер1 - tr_stng в 1(true)                               *
 * иначе возвращает 0                                                         *
 */
int is_start_strconst(char s[], unsigned i) {
  if (!(tr_comm || tr_stng) && '\'' == s[i]) {
    tr_sdtc = YES;
    return YES;
  }
  return NO;
}

/* ф-ция: is_str_strconst_comm                                                *
 * пар-ры:                                                                    *
 * НЕТ                                                                        *
 * глобальные переменные:                                                     *
 * tr_comm      -- триггер1 - признак нахождения в комментарии                *
 * tr_stng      -- триггер2 - признак нахождения в строке                     *
 * tr_sdtc      -- триггер3 - признак нахождения в символьной константе       *
 * возвращает 1(true) если установлен один из признаков комментарий, строка   *
 * или символьная константа                                                   *
 * иначе возвращает 0                                                         *
 */
int is_str_strconst_comm(void) {
  return tr_comm || tr_stng || tr_sdtc;
}

/* ф-ция: puts_without_commect:                                               *
 * t[]          --  массив символов исходной строки (одна или больше строк)   *
 *                  результат                                                 *
 * s[]          --  исходная строка                                           *
 */
void copy_with_remove_comment(char t[], char s[]) {
  unsigned li = 0;

  while (s[li] != EOL) {
    if (NL == s[li]) {
      t[bi++] = s[li++];
    } else if (is_end_comm(s, li)) { /* is_end_comm()  */
      /* триггер1 установлен и конец комментария */
      li = li + 2;
    } else if (is_double_backslash_in_str(s, li)) {
      /* триггер2 или триггер3 установлен и найден паттерн \\ */
      t[bi++] = s[li++];
      t[bi++] = s[li++];
    } else if (is_double_qoutmark_in_str(s, li)) {
      /* триггер2 установлен и найден паттерн \" */
      t[bi++] = s[li++];
      t[bi++] = s[li++];
    } else if (is_single_qoutmark_in_stc(s, li)) {
      /* триггер3 установлен и найден паттерн \' */
      t[bi++] = s[li++];
      t[bi++] = s[li++];
    } else if ('\\' == s[li] && '\n' != s[li+1]) {
      /* найден паттерн \\n */
      li = li + 2;
    } else if (is_end_str(s, li)) {
      /* триггер2 установлен и найден символ " */
      t[bi++] = s[li++];
    } else if (is_end_stc(s, li)) {
      /* триггер3 установлен и найден символ ' */
      t[bi++] = s[li++];
    } else if (is_start_comm(s, li)) {
      /* не в строке или символьной константе
       * и начало комментария */
      li = li + 2;
    } else if (is_start_string(s, li)) {
      /* не в комментарии и не установлен триггер3 и начало строки */
      t[bi++] = s[li++];
    } else if (is_start_strconst(s, li)) {
      /* не в комментарии и не установлен триггер2 и
       * начало символьной константы */
      t[bi++] = s[li++];
    } else if (!tr_comm) { /* если не в комментарии */
      /* выводим текущий символ и инкриминируем индекс */
      t[bi++] = s[li++];
    } else if (NL == s[li]) {
      t[bi++] = s[li++];
    } else {
      li++; /* двигаемся по комментарию */
      t[bi++] = ' ';
    }
    /* переходим на следующий шаг цикла */
  }
  t[bi] = 0;
}

/* stack pointer                                                             */
unsigned _stack_pointer = 0;         /*                      указатель стека */
char     _stack_symbol[MAX_STACK];   /*           массив для стека элементов */
unsigned _stack_pos_line[MAX_STACK]; /*массив позиции(строки) элементов стека*/
unsigned _stack_pos_col[MAX_STACK];  /*    массив позиции(колонки) элементов */

/* ф-ция: stack_push                                                          *
 * пар-ры:                                                                    *
 * s            -- символ/знак который помещаем на вершину стека              *
 * i            -- текущая позиция в во входном потоке                        *
 * глобальные переменные:                                                     *
 *              -- нет.                                                       *
 * возвращает: следующая позиция в стеке она больше нуля == (true)            *
 * если стек переполнен возвращаем 0 == (false)                               *
 */
unsigned stack_push(char s, unsigned l, unsigned c) {
  unsigned p;

  if (MAX_STACK > _stack_pointer) {
    p = _stack_pointer;
    _stack_symbol[_stack_pointer] = s;
    _stack_pos_line[_stack_pointer] = l;
    _stack_pos_col[_stack_pointer] = c;
    _stack_pointer++;
    return _stack_pointer;
  }
  return 0;
}

/* ф-ция: stack_pop                                                           *
 * пар-ры:                                                                    *
 *              -- нет.                                                       *
 * глобальные переменные:                                                     *
 *              -- нет.                                                       *
 * возвращает: извлекаемый символ                                             *
 * если стек исчерпан возвращаем 0 == (false)                                 *
 */
char stack_pop(void) {
  if ( _stack_pointer > 0 ) {
    _stack_pointer--;
    _stack_symbol[_stack_pointer] = 0;
    _stack_pos_line[_stack_pointer] = 0;
    _stack_pos_col[_stack_pointer] = 0;
    return _stack_symbol[_stack_pointer];
  }
  return 0;
}

/* ф-ция: stack_pointer                                                       *
 * пар-ры:                                                                    *
 *              -- нет.                                                       *
 * глобальные переменные:                                                     *
 *              -- нет.                                                       *
 * возвращает: позицию после добавленного элемента                            *
 */
unsigned stack_pointer(void) {
  return _stack_pointer;
}

/* ф-ция: stack_top_line                                                      *
 * пар-ры:                                                                    *
 *              -- нет.                                                       *
 * глобальные переменные:                                                     *
 *              -- нет.                                                       *
 * возвращает: значение позиции(номер строки) в потоке, верхнего элемента     *
 */
unsigned stack_top_line(void) {
  if (_stack_pointer > 0) {
    return _stack_pos_line[_stack_pointer - 1];
  }
  return 0;
}

/* ф-ция: stack_top_line                                                      *
 * пар-ры:                                                                    *
 *              -- нет.                                                       *
 * глобальные переменные:                                                     *
 *              -- нет.                                                       *
 * возвращает: значение позиции(номер колонки) в потоке, верхнего элемента    *
 */
unsigned stack_top_col(void) {
  if (_stack_pointer > 0) {
    return _stack_pos_col[_stack_pointer - 1];
  }
  return 0;
}

/* ф-ция: stack_top_line                                                      *
 * пар-ры:                                                                    *
 *              -- нет.                                                       *
 * глобальные переменные:                                                     *
 *              -- нет.                                                       *
 * возвращает: значение верхнего элемента                                     *
 */
char stack_top(void) {
  if (_stack_pointer > 0) {
    return _stack_symbol[_stack_pointer - 1];
  }
  return 0;
}

/* ckr_getline: read a line into s, return length */
/* ckr_getline: читает строку и возвращает ее длину */
int ckr_getline(char s[], int lim) {
  int c, i;

  for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
    s[i] = c;
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';
  return i;
}

/* strlen: return length of s */
/* strlen: возвращает длину строки s */
int ckr_strlen(char s[])
{
  int i;
  i = 0;
  while (s[i] != '\0')
    ++i;
  return i;
}

/* vim: syntax=c:paste:ff=unix:textwidth=78:ts=2:sw=2
 * EOF */
