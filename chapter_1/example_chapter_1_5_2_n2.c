/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* count characters in input; 2st version */
/* Подсчет вводимых символов. Версия 2 */

#include <stdio.h>

main()
{
  double nc;
  nc = 0;
  for (nc = 0; getchar() != EOF; ++nc)
    ;
  printf("%.0f\n", nc);
}
