/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 0$
 */
/* copy input to output; 1st version */ 
/* Простейший пример — программа, копирующая по одному символу из входного потока
 * в выходной:
 *   Чтение символа
 *   while(символ не является признаком конца файла)
 *     вывод только что прочитанного символа
 *     чтение символа
 **/

#include <stdio.h>

main()
{
  int c;
  c = getchar();
  while (c != EOF) {
    putchar(c);
    c = getchar();
  }
}
