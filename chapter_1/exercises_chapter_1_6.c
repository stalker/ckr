/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* Exercsise 1-6. Verify that the expression getchar() != EOF is 0 or 1. */
/* Провеьте, что значение выражения getchar != EOF равно 0 или -1. */

#include <stdio.h>

main() {
  int c;
  c = getchar();
  int result = (c != EOF);
  if (-1 == c)
    printf(" (getchar() == EOF) = %d\n", result);
  else
    printf(" (getchar() != EOF) = %d\n", result);
  while (c != EOF) {
    putchar(c);
    c = getchar();
    result = (c != EOF);
    if (-1 == c)
      printf(" (getchar() == EOF) = %d\n", result);
    else
      printf(" (getchar() != EOF) = %d\n", result);
  }
}
/* EOF */
