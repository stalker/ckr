/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* Exercise 1-5. Modify the temperature conversion program to print the table 
 * in reverse order, that is, from 300 degrees to 0. */
/* Измените программу преобразования температур так, чтобы она печатала таблицу
 * в обратном порядке, т.е. от 300 до 0.
*/

#include <stdio.h>

main()
{
  int fahr;
  printf("%4s %7s\n", "fahr", "celsius");
  for (fahr = 300; fahr >= 0; fahr = fahr - 20) 
    printf("%3d %6.1f\n", fahr, (5.0/9.0)*(fahr-32));
}
/* EOF */
