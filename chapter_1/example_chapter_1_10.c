/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* 1.10 External Variables and Scope
 * 1.10. Внешние переменные и область видимости
 */
#include <stdio.h>

/* максимальный размер вводимой строки */
#define MAXLINE 255 /* maximum input line length */

/* длина максимальной из просмотренных строк */
int max;                  /* maximum length seen so far */
/* текущая строка */
char line[MAXLINE];       /* current input line */
/* самая длинная строка */
char longest[MAXLINE];    /* longest line saved here */

int get_line(char line[], int maxline);
void copy(char to[], char from[]);

/* print the longest input line */
/* Печать самой длинной строки. Версия 2 */
int main() {
  /* длина текущей строки */
  int len;                                   /* current line length */
  extern int max;
  extern char longest[];

  max = 0;
  while ((len = get_line(line, MAXLINE)) > 0)
  if (len > max) {
    max = len;
    copy(longest, line);
  }
  /* Если была хотя бы одна строка */
  if ( max > 0) /*there was a line*/
    printf("%s", longest);
  return 0;
}

/* get_line: read a line into s, return length */
/* getline: читает строку и возвращает ее длину */
int get_line(char s[],int lim) {
  int c, i;
  extern char line[];

  for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
    s[i] = c;
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';
  return i;
}

/* copy: copy 'from' into 'to'; assume to is big enough */
/* copy: копирует строку */
void copy(char to[], char from[]) {
  int i;
  extern char line[], longest[];

  i = 0;
  while ((to[i] = from[i]) != '\0')
    ++i;
}

/* vim: syntax=c:paste:ff=unix:textwidth=78:ts=2:sw=2
 * EOF */
