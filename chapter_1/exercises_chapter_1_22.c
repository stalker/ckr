/* $Date$
 * $Id$
 * $Version: 0.6$
 * $Revision: 1$
 */
/* 1.10 External Variables and Scope
 * 1.10. Внешние переменные и область видимости
 */
/* Exercise 1-22. Write a program to ``fold'' long input lines into two or
 * more shorter lines after the last non-blank character that occurs before
 * the n-th column of input. Make sure your program does something intelligent
 * with very long lines, and if there are no blanks or tabs before the
 * specified column.
 */
/* Упражнение 1.22. Напишите программу для сворачивания слишком длинных строк
 * входного потока в две или более коротких строки после последнего непустого
 * символа, встречающегося перед n-м столбцом длинной строки. Постарайтесь,
 * чтобы ваша программа обрабатывала очень длинные строки корректно, а также
 * удаляла лишние пробелы и табуляции перед указанным столбцом.
 */

#include <stdio.h>

#define MAXLINE 255                   /* максимальная длина строки в потоке */
#define MAX_UI  255                   /* максимальная длина строки в потоке */

const int n = 10;
int ncol;

enum escapes { BACKSPACE = '\b', NEWLINE = '\n', RETURN = '\r' };
enum escs    { EOL = '\0', BELL = '\a', BS = '\b', TAB = '\t',
               NL = '\n', VTAB = '\v', RETN = '\r', SP = ' ' };

int my_split(char to[], char from[], int len);
unsigned int ui_chomp(char *s);
int ckr_getline(char s[], int lim);

int main() {
  int i = 0;
  int len;                                          /* длина текущей строки */
  char in_line[MAXLINE];                        /* текущая введенная строка */
  char out_line[MAXLINE];                      /* массив символов/результат */
  ncol = n;

  while ((len = ckr_getline(in_line, MAXLINE)) > 0) {
    i++;
    my_split(out_line, in_line, len);
    ui_chomp(in_line);
    printf("%s", out_line);
  }

  return 0;
}

/* my_split: функция сворачивает слишком длинные строки длинной не более n
 *           символов (если единственное слово в строке длинне n, оно не
 *           разбивается!).
 *           Разделитель строки пустой (пробельный SP = ' ' TAB = '\t') символ
 * from   -- исходная строка
 * to     -- массив символов исходной строки (одна или больше строк) результат
 */
int my_split(char to[], char from[], int len) {
  int bi = 1; /* block index */
  int eb = 0; /* end position of current block */
  int sb = 0; /* block position */
  int sp = 0; /* next space position */
  int ti = 0; /* target(to[]) index */

  /* цикл по блокам делаем пока не достигнем конца входной строки */
  while (sb < len) {
    eb = end_of_block(from, sb, len); /* определяем границу блока */
    sp = space_position(from, sb, eb, len);
    if (0 != sp) {
      ti = copy_block(to, from, sb, sp, ti);
      to[ti++] = NL;
      sb = sp + 1;
    } else {                               /* не нашли SP или TAB */
      ti = copy_block(to, from, sb, len, ti);
      sb = len;
    }
    bi++;
  }
  to[ti] = EOL;

  return ti;
}

/* end_of_block: функция вычисляет позицию конца блока учитывая табудяцию
 * s      -- исходная строка
 * sb     -- start position of current block
 * len    -- lenght of s
 */
int end_of_block(char s[], int sb, int len) {
  int i = 0, k = 0;
  for (i = sb; i <= len && k < ncol; i++) {
    if (TAB == s[i]) {
      k += 8;
    } else k++;
  }
  return --i;
}

/* space_position: определяет позицию ближайшего к eb пробельного символа
 *                 (справа или слева от eb)
 * from[] -- исходная строка
 * sb     -- начало блока
 * eb     -- конец блока
 * len    -- длина строки
 * если пробельный символ не найден от позиции sb до конца строки
 * возвращается 0
 */
int space_position(char from[], int sb, int eb, int len) {
  int fi = 0; /* source(from[]) index */
  int i, incr = -1;
  for (i = 0; i < 2; i++) {
    fi = eb;
    while (sb <= fi && fi < len) {
      if (SP == from[fi] || TAB == from[fi]) {
        return fi;
      } else fi = fi + incr;
    }
    incr = incr * incr;
  }
  return 0;
}

/* copy_block: копируем кусок текущего блока из source в target
 * from -- source строка
 * to   -- target строка
 * sb   -- начало блока
 * eb   -- конец блока ( не включая! )
 */
int copy_block(char to[], char from[], int sb, int eb, int ti) {
    /* копируем кусок текущего блока из source в target */
    int fi;
    fi = sb;
    while (fi < eb) {
      to[ti++] = from[fi++];
    }
    return ti;
}

/* ui_chomp: removes any trailing string that corresponds to the current
 * value */
unsigned int ui_chomp(char *s) {
  unsigned int i;

  if (!s || !*s) { return 0; }

  while (s[1]) {
    if (MAX_UI < i) {
      *s = EOL;
    } else {
      ++i; ++s;
    }
  }

  if (*s != NL) { return 0; }
  *s = 0;

  return NL;
}

/* ckr_getline: read a line into s, return length */
/* ckr_getline: считывает строку в s, возвращает ее длину */
int ckr_getline(char s[], int lim) {
  int c, i;

  for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
    s[i] = c;
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';
  return i;
}

/* vim: syntax=c:paste:ff=unix:textwidth=78:ts=2:sw=2
 * EOF */
