/* $Date$
 * $Id$
 * $Version: 0.1$ 
 * $Revision: 1$
 */
/* count lines, words, and characters in input */
/* 1.5.4. Подсчет слов
 * Четвертая из нашей серии полезных программ подсчитывает строки, слова 
 * и символы, причем под словом здесь имеется в виду любая последовательность 
 * символов, не содержащая пробелов, табуляций и символов новой строки. 
 * Эта программа является упрощенной версией программы wc системы UNIX.
 */

#include <stdio.h>

#define IN 1 /* inside a word */ 
#define OUT 0 /* outside a word */

main() {
  int c, nl, nw, nc, state;
  state = OUT;
  nl = nw = nc = 0;
  while ((c = getchar()) != EOF) {
    ++nc;
    if (c == '\n')
      ++nl;
    if (c == ' ' || c == '\n' || c == '\t')
      state = OUT;
    else if (state == OUT) {
      state = IN;
      ++nw; 
    }
  }
  printf("%d %d %d\n", nl, nw, nc); 
}
