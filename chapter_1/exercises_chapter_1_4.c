/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* print Celsius-Fahrenheit table
 * for celsius = -10, 0, 10, 20, ..., 300 floating-point version */
/* Напишите программу, которая будет печатать таблицу соответствия температур
 * по Цельсию температурам по Фаренгейту.
 */

#include <stdio.h>

main() {
  float fahr, celsius; float lower, upper, step;
  lower = -10; 	/* lower limit of temperatuire scale */
  upper = 300; 	/* upper limit */
  step = 10;	/* step size */
  celsius = lower;
  printf("%6s %4s\n", "celsius", "fahr");
  while (celsius <= upper) {
    fahr = (9.0*celsius + 160.0) / 5.0;
    celsius = (5.0/9.0) * (fahr-32.0);
    printf("%6.1f  %5.1f\n", celsius, fahr);
    celsius = celsius + step;
  }
}
/* EOF */
