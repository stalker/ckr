/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */
/* 1.9 Character Arrays
 *     Finally, we need a main program to control getline and copy. 
 *     Here is the result.
 * 1.9. Массивы символов
 *     Наконец, нам необходима главная программа, которая управляет 
 *     функциями getline и copy. Вот как выглядит программа в целом:
 */

#include <stdio.h>

#define MAXLINE 1000 /* maximum input line length */

int get_line(char line[], int maxline);
void copy(char to[], char from[]);

/* print the longest input line */
int main() {
  int len;					/* current line length */        
  int max;                  /* maximum length seen so far */
  char line[MAXLINE];       /* current input line */
  char longest[MAXLINE];    /* longest line saved here */

  max = 0;
  while ((len = get_line(line, MAXLINE)) > 0)
  if (len > max) {
    max = len;
    copy(longest, line);
  }
  if ( max > 0) /*there was a line*/
    printf("%s", longest);
  return 0;
}

/* get_line: read a line into s, return length */
int get_line(char s[],int lim) {
  int c, i;
  for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
    s[i] = c;
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';
  return i;
}

/* copy: copy 'from' into 'to'; assume to is big enough */
void copy(char to[], char from[]) {
  int i;
  i = 0;
  while ((to[i] = from[i]) != '\0')
    ++i; 
}

/* vim: syntax=c:paste:ff=unix:textwidth=78:ts=2:sw=2
 * EOF */
