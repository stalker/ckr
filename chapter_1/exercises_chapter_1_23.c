/* $Date$
 * $Id$
 * $Version: 0.2$
 * $Revision: 1$
 */
/* 1.10 External Variables and Scope
 * 1.10. Внешние переменные и область видимости
 */
/* Exercise 1-23. Write a program to remove all comments from a C program.
 * Don't forget to handle quoted strings and character constants properly.
 * C comments don't nest.  */
/* Упражнение 1.23. Напишите программу для удаления всех комментариев из
 * программы на С. Позаботьтесь о корректной обработке символьных констант
 * и строк в двойных кавычках. Вложенные комментарии в С не допускаются.
 */

#include <stdio.h>

#define MAXLINE 255 /* maximum input line length */
#define notcomment '/*'

char testline1[] = " /* это не комментарий */ ";

void puts_without_commect(char s[]);
int ckr_getline(char s[],int lim);

enum escs    { EOL = '\0', BELL = '\a', BS = '\b', TAB = '\t',
               NL = '\n', VTAB = '\v', RETN = '\r', SP = ' ' };

/*
 * tr: тригер1 0 если "мы в комментрации"
 *             1 если "мы не в комментрации"
 * ts: тригер2 0 если "мы в строке ограниченной символом(") "
 *             1 если "мы не в строке"
 * tc: тригер3 0 если "мы в строке ограниченной символом(') "
 *             1 если "мы не в строке"
*/
int tr, ts, tc;

int main() {
  int i = 0;
  int len;                     /*current line length / длина текущей строки */
  char in_line[MAXLINE];                        /* текущая введенная строка */

  tr = ts = tc = 0;

  while ((len = ckr_getline(in_line, MAXLINE)) > 0) {
    i++;
    puts_without_commect(in_line);
  }

  return 0;
}

/* функция: puts_without_commect:
 * s    -- s[] входная строка.
 * выводит тело c-файла без комментариев.
 */
void puts_without_commect(char s[]) {
  int li = 0;

  /* цикл_до_конца_строки1 обрабатываем строку s её индекс li */
  while (s[li] != EOL) {
    /* если1 тригер1 установлен проверяем паттерн
     * '*' == s[li] && '/' == s[li+1] */
    if (tr && '*' == s[li] && '/' == s[li+1]) {
      /* тригер1 установлен и конец комментария */
      tr = 0;
      li = li + 2;
    } else if (!tr && (ts || tc) && '\\' == s[li] && tc && '\\' == s[li+1]) {
      /* иначе если1 не в комментарии и
       * тригер2 или тригер3 установлен и найден паттерн \\ */
      putchar(s[li++]);
      putchar(s[li++]);
    } else if (!tr && ts && '\\' == s[li] && tc && '"' == s[li+1]) {
      /* иначе если1 не в комментарии и тригер3
       * тригер2 установлен и найден паттерн \" */
      putchar(s[li++]);
      putchar(s[li++]);
    } else if (!tr && ts && '"' == s[li] && '\\' != s[li-1]) {
      /* иначе если1 не в комментарии и
       * тригер2 установлен и найден символ '"' */
      ts = 0;
      putchar(s[li++]);
    } else if (!tr && tc && '\\' == s[li] && tc && '\'' == s[li+1]) {
      /* иначе если1 не в комментарии и
       * тригер3 установлен и найден паттерн \' */
      putchar(s[li++]);
      putchar(s[li++]);
    } else if (!tr && tc && '\'' == s[li]) {
      /* иначе если1 не в комментарии и
       * тригер3 установлен и найден символ ' */
      tc = 0;
      putchar(s[li++]);
    } else if (!(tr || ts || tc) && '/' == s[li] && '*' == s[li+1]) {
      /* не в строке или символьной константе
       * и начало комментария */
      tr = 1;
      li = li + 2;
    } else if (!(tr || tc) && '"' == s[li]) {
      /* не в комментарии и не установлен тригер3 и начало строки */
      ts = 1;
      putchar(s[li++]);
    } else if (!(tr || ts) && '\'' == s[li]) {
      /* не в комментарии и не установлен тригер2 и
       * начало символьной константы */
      tc = 1;
      putchar(s[li++]);
    } else if (!tr) { /* если не в комментарии */
      /* выводим текущиц сивол и инкрементируем индекс */
      putchar(s[li++]);
    } else li++; /* двигаемся по комментарию */
    /* переходим на следующий шаг цикла */
  }
}

/* ckr_getline: read a line into s, return length */
/* ckr_getline: читает строку и возвращает ее длину */
int ckr_getline(char s[],int lim) {
  int c, i;

  for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
    s[i] = c;
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';
  return i;
}

/* vim: syntax=c:paste:ff=unix:textwidth=78:ts=2:sw=2
 * EOF */
