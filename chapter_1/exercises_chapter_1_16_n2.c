/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 1$
 */

/* программа определения самой длинной строки */
/* Упражнение 1.16. Доработайте главный модуль программы определения самой
 * длинной строки так, чтобы она выводила правильное значение для какой
 * угодно длины строк входного потока, насколько это позволяет текст. */
/* В непереведенном варианте задача звучит так:
 * "Revise the main routine of the longest-line program so it will correctly
 * print the length of arbitrarily long input lines, and as much as possible
 * of the text.",
 * а в примере программы есть вывод количества символов в самой длинной строке.
 * То есть надо исправить функцию main, чтобы программа корректно выводила
 * количество символов самой длинной строки и максимально возможное количество
 * текста (ограничение - MAXLINE).
 */

#include <stdio.h>

#define MAXLINE 1000 /* максимальная длина строки в потоке */

int get_line(char line[], int maxline);
void copy(char to[], char from[]);

/* вывод самой длинной строки в потоке */
int main()
{
  int len; /* длина текущей строки */
  int max; /* текущая максимальная длина */
  char line[MAXLINE]; /* текущая введенная строка */
  char longest[MAXLINE]; /* самая длинная строка из введенных */
  int c;

  max = 0;
  while ((len = get_line(line, MAXLINE)) > 0) {
    /* если строка больше MAXLINE, продолжаем считаем количество символов */
    if (line[len-1] != '\n')
      while ((c = getchar()) != EOF && c != '\n')
        ++len;
      if (len > max) {
        max = len;
        copy(longest, line);
      }
    }

  /* если строка короче MAXLINE, то функция get_line добавляет
  в конце строки '\0', который нам считать не нужно */
  if (max < MAXLINE)
    --max;

  if (max > 0) { /* была непустая строка */
    if (max > len)
      printf("The longest line is:%s ...\n", longest);
    else printf("The longest line is: %s\n", longest);
      printf("This line consists of %d characters\n", max);
  }

  return 0;
}

/* get_line: считывает строку в s, возвращает ее длину */
int get_line (char s[], int lim)
{
  int c, i;

  for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
    s[i] = c;

  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';

  return i;
}

/* copy: копирует строку 'from' в 'to'; длина to считается достаточно точной */
void copy(char to[], char from[])
{
  int i = 0;

  while ((to[i] = from[i]) != '\0')
    ++i;
}

/* vim: syntax=c:paste:ff=unix:textwidth=78:ts=2:sw=2
 * EOF */
