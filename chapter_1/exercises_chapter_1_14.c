/* $Date$
 * $Id$
 * $Version: 0.1$
 * $Revision: 0$
 */
/* 1.6 Arrays
 * Write a program to print a histogram of the frequencies of different
 * characters in its input. */
/* Упражнение 1.14. Напишите программу для вывода гистограммы частот,
 * с которыми встречаются во входном потоке различные символы.
 */

#include <stdio.h>

#define SP ' '                              /* отбрасываем невидимые символы */
#define MAX_REP_CHAR 128                       /* в гистограмме только ASCII */

const unsigned char max_size = 255;               /* считаем все символы     *
                                                   * (байты как часть UTF-8) */
int main() {
  unsigned char c;                                         /* текущий символ */
  int i, j;                                                      /* счётчики */
  int nc;                            /* счётчик для не ASCII символов/байтов */
  int ndigit[max_size];                            /* массив для гистограммы */

  /* инициируем переменные */
  nc = 0;
  for (i = 0; i < max_size; ++i)
     ndigit[i] = 0;

  while ((c = getchar()) != (unsigned char)EOF)                    /* читаем */
    ++ndigit[c];                                                  /* считаем */

  for (i = SP; i < MAX_REP_CHAR; i++) {   /* печатаем виденные ASCII символы */
    if ( ndigit[i] > 0 ) {                  /* только те которые "встретили" */
      putchar(i);
      putchar(':');
      putchar(' ');
      for (j = 0; j < ndigit[i]; j++)           /* выводим саму гистограмму */
        putchar('X');
      putchar('\n');
    }
  }
  for (i = MAX_REP_CHAR; i < max_size; ++i) {    /* считаем не ASCII символы */
     nc = nc + ndigit[i];
  }
  printf("over code %d: %d", MAX_REP_CHAR - 1, nc );    /* выводим общую для */
  for (j = 0; j < nc; j++)                    /* всех не ASCII символов/байт */
	putchar('X');
  putchar('\n');
}
/* EOF */
