#################################################################################
# $Date$
# $Id$
# $Version: 0.7$
# $Revision: 7$
### expand_date #################################################################
#!/usr/bin/env ruby
data = STDIN.read
last_date = `git log --pretty=format:"%ad" -1`
datepat='^# \$Date\$'
puts data.gsub(/#{datepat}/, '#'+' $Date: ' + last_date.to_s + '$')
#EOF
#################################################################################
$ git config filter.dater.smudge expand_date
$ git config filter.dater.clean 'perl -pe "s|^(# +\\\$Date): .*\\\$|\1\\\$|"'
#################################################################################
### expand_date_c ###############################################################
#!/usr/bin/env ruby
data = STDIN.read
last_date = `git log --pretty=format:"%ad" -1`
puts data.gsub('/* $Date$', '/* $Date: ' + last_date.to_s + '$')
#EOF
#################################################################################
$ git config filter.daterc.smudge expand_date_c
$ git config filter.daterc.clean 'perl -pe "s|(/\* \\\$Date): .*\\\$|\1\\\$|"'
##############################################################################EOF
